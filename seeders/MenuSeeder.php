<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\Category;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu:: create([
        	'name' => 'coffe',
        	'category_id' => '1',
        	'description' => 'enak',
        	'price' => '5000'
        	
        ]);

        $Category = Category::first();
        factory(Menu::class, 1)->create(

    	[
    		'category_id' => $Category->id,
    	]

     );

   }

}
