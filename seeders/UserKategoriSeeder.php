<?php

use Illuminate\Database\Seeder;
use App\Models\Kategori;


class UserKategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        // DB::table('kategori')->insert([
        //     'name'=> 'minuman',
        // ]);

        factory(Kategori::class,3)->create();
    }
}
